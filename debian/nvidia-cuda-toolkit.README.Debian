nvidia-cuda-toolkit for Debian
------------------------------
Contents:
  1. Getting NVCC Working with the Compiler
  2. Installing the CUDA Code Samples

Getting NVCC Working with the Compiler
======================================

It is worth noting that not all compilers can be used together with NVCC.
Here is a list of possible combinations, partially based on [1]:

    ```
    CUDA Toolkit Version | Supported GCC Versions | Supported Clang Versions
    ---------------------+------------------------+-------------------------
     12.6                | 6 - 13                 | 7 - 18
     12.5                | 6 - 13                 | 7 - 17
     12.4                | 6 - 13                 | 7 - 17
     12.3                | 6 - 12                 | 7 - 16
     12.2.2              | 6 - 12                 | 7 - 16
     12.2                | 6 - 12                 | 7 - 15
     12.1                | 6 - 12                 | 6.0 - 15
     12.0                | 6 - 12                 | 6.0 - 14
     11.8                | 6 - 11                 | 6.0 - 14
     11.7                | 6 - 11                 | 6.0 - 13
     11.6                | 6 - 11                 | 6.0 - 12
     11.5                | 6 - 11.1 *)            | 6.0 - 12
     11.4.1              | 6 - 11.1 *)            | 6.0 - 11
     11.4                | 6 - 10                 | 6.0 - 11
     11.3                | (5) 6 - 10             | (3.8) 6.0 - 11
     11.2                | (5) 6 - 10             | (3.8) 6.0 - 11
     11.1                | (5) 6 - 10             | (3.8) 6.0 - 10
     11.0                | (5) 6 - 9              | (3.8) 6.0 - 9
     10.2                | 5 - 8                  | 3.8 - 8
     10.1 Update 1       | 5 - 8                  | 3.8 - 8
     10.1                | 5 - 8                  | 3.8 - 7
     10.0                | 5 - 7                  | 3.8 - 6.0
      9.2                | 5 - 7                  | 3.8 - 5.0
      9.1                | 5 - 6                  | 3.8 - 4.0
      9.0                | 5 - 6                  | 3.8 - 3.9
      8.0                | 5                      | 3.8
      7.5                | 4.9 (5)                | 3.6
    ```

*) g++ 11.0 and 11.1 works fine, g++ 11.2 or later causes failures when
   compiling certain STL headers with nvcc

[1] https://gist.github.com/ax3l/9489132


Installing the CUDA Code Samples
=================================

The CUDA Code Samples are no longer distributed with the CUDA Toolkit
but in the github repository https://github.com/nvidia/cuda-samples

They are also available in the nvidia-cuda-samples package which has
dependencies on all -dev packages needed to build them.

For more information on the Samples, please see
https://developer.nvidia.com/cuda-code-samples

 -- Andreas Beckmann <anbe@debian.org>  Thu, 24 Nov 2022 11:22:45 +0100
